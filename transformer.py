import rdflib
import pandas as pd
from rdflib import Graph, Literal, RDF, URIRef
from rdflib.namespace import XSD

# Namespace definitions
SAREF = rdflib.Namespace("https://w3id.org/saref#")

#  Clean the dataset by removing non-numeric characters from data columns
#  that are expected to contain only numeric values
def clean_data(data):
    for column in data.columns:
        if column not in ['utc_timestamp', 'cet_cest_timestamp']:
            # Remove non-numeric characters and convert strings to NaN where conversion fails
            data[column] = pd.to_numeric(data[column].astype(str).replace('[^0-9.-]', '', regex=True), errors='coerce')

def create_triples(graph, data):
    for index, row in data.iterrows():
        timestamp = row['utc_timestamp']
        
        for column in data.columns:
            if column not in ['utc_timestamp', 'cet_cest_timestamp']:
                # Create unique URIs for each measurement, device, and property using the index and column name.
                measurement_uri = URIRef(f"http://example.org/measurement/{index}_{column}")
                device_uri = URIRef(f"http://example.org/device/{column}")
                property_uri = URIRef(f"http://example.org/property/{column}")
                
                # RDF type for measurement
                graph.add((measurement_uri, RDF.type, SAREF.Measurement))
                
                # Timestamp
                if not pd.isna(timestamp):
                    graph.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                
                # Validate and add measurement value if it's a valid decimal
                if pd.notna(row[column]):
                    graph.add((measurement_uri, SAREF.hasValue, Literal(row[column], datatype=XSD.decimal)))
                else:
                    print(f"Missing data at row {index}, column {column}")
                
                # Link measurement to device and property
                graph.add((measurement_uri, SAREF.isMeasuredBy, device_uri))
                graph.add((measurement_uri, SAREF.relatesToProperty, property_uri))
                
                # Add unit of measure based on the column name
                if "power" in column:
                    unit_uri = URIRef("http://example.org/unit/Watt")
                    graph.add((measurement_uri, SAREF.hasUnitOfMeasure, unit_uri))
                elif "energy" in column:
                    unit_uri = URIRef("http://example.org/unit/kWh")
                    graph.add((measurement_uri, SAREF.hasUnitOfMeasure, unit_uri))
                else:
                    unit_uri = URIRef("http://example.org/unit/unit")
                    graph.add((measurement_uri, SAREF.hasUnitOfMeasure, unit_uri))
                
                # Define the device
                graph.add((device_uri, RDF.type, SAREF.Device))
                
                # Define the property
                graph.add((property_uri, RDF.type, SAREF.Property))

#Main function to create the triples and turtle file
def main(filepath):
    data = pd.read_csv(filepath, sep=',')
    clean_data(data)  # Clean the data first
    graph = Graph()
    graph.bind("saref", SAREF)
    create_triples(graph, data) #call the function to create the triples
    graph.serialize(destination="graph.ttl", format="turtle") #save everything on a turtle file
